package automation.stepdefs;
import automation.pages.ContactPage;
import automation.pages.KzaHeader;
import automation.services.XReporter;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static automation.ui.SharedDriver.*;

/**
 * Created by KK on 5/5/2017.
 */
public class KZASteps {

    private final Logger logger = LoggerFactory.getLogger(KZASteps.class);

    private XReporter xReport;
    private boolean finalTestInSuite=false;
    private KzaHeader header;
    private ContactPage contactPage;

    public KZASteps(KzaHeader header, ContactPage contactPage){
        this.header=header;
        this.contactPage=contactPage;
        xReport = new XReporter(featureAdmin);
    }

    private void setFinalTestInSuite(){
        finalTestInSuite=true;
    }

    private void closeScenario() {
        xReport.logScenario("p", "pass");
        if (finalTestInSuite) {
            xReport.logFeature("p", "pass");
        }
    }

    @Given("^I open the ZKA website$")
    public void iOpenTheKZAWebsite() throws Throwable {
        xReport.newScenario("See admin button");
        xReport.newStep("I open the KZA website","Given");
        header.openwebsite(xReport);
    }

    @When("^I click on the contact button$")
    public void iClickOnTheContactButton() throws Throwable {
        xReport.newStep("I click on the contact button","When");
        header.navigateToContactPage(xReport);
    }

    @Then("^I expect to see the correct kza address$")
    public void iExpectToSeeTheKza_address() throws Throwable {
        xReport.newStep("I expect to see the correct kza_address","Then");
        contactPage.validateAddressKza(xReport);
        setFinalTestInSuite();
        closeScenario();
    }


}
