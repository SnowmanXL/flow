package automation.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class KzaDependance {

    private String name;
    private String streetName;
    private String postalCode;
    private String phoneNumber;
    private String emailAddress;
    private String city;

    public KzaDependance() {
        this("KZA", "Tolweg 5","3741 LM","+31 35 543 1000","info@kza.nl","Baarn");
    }

    public KzaDependance(String name, String streetName, String postalCode, String phoneNumber, String emailAddress, String city) {
        this.name = name;
        this.streetName = streetName;
        this.postalCode = postalCode;
        this.phoneNumber = "T: "+phoneNumber;
        this.emailAddress = "E: "+emailAddress;
        this.city = city;
    }

    public List<String> getAllAttributesAsList(){
        List<String> attributes = new ArrayList<>();

        //attributes.add(getName());
        attributes.add(getStreetName());
        attributes.add(getPostalCode());
        attributes.add(getCity());
        attributes.add(getPhoneNumber());
        attributes.add(getEmailAddress());

        return attributes;
    }

    public String getName() {
        return name;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCity() {
        return city;
    }

}
