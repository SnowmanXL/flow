package automation.model;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class Employee {
    private String firstName;
    private String lastName;
    private String fullName;


    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        fullName = this.firstName + " " + this.lastName;
    }

    public String getFullName() {
        return fullName;
    }

}
