package automation.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Wluijk on 6/1/2017.
 */
public class TestResult {
    private String timeStamp;
    private File screenShot;
    private Throwable errorStackTrace;
    private String updateText;
    private String testStatus;

    public static final String TESTPASS = "p";
    public static final String TESTFAIL = "f";
    public static final String TESTWARN = "w";
    public static final String TESTINFO = "i";


    public TestResult(File screenShot, Throwable errorStackTrace, String updateText, String testStatus) {
        timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        this.screenShot = screenShot;
        this.errorStackTrace = errorStackTrace;
        this.updateText = updateText;
        this.testStatus = testStatus;
    }

    public TestResult(File screenShot, String updateText, String testStatus) {
        this(screenShot,null,updateText,testStatus);
    }

    public TestResult(String updateText, String testStatus) {
        this(null,null,updateText,testStatus);
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public File getScreenShot() {
        return screenShot;
    }

    public Throwable getErrorStackTrace() {
        return errorStackTrace;
    }

    public String getUpdateText() {
        return updateText;
    }

    public String getTestStatus() {
        return testStatus;
    }
}
