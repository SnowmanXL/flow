package automation.services;

import automation.model.TestResult;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.GherkinKeyword;
import com.aventstack.extentreports.gherkin.model.Scenario;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import java.io.File;
import java.util.List;

import static automation.model.TestResult.*;

/**
 * Created by Wluijk on 4/18/2017.
 */
public class XReporter {
    private ExtentTest xFeature;
    private ExtentTest xScenario;
    private ExtentTest xStep;


    public XReporter(ExtentTest feature) {
        xFeature = feature;
    }

    public void newScenario(String scenarioName){
        xScenario = xFeature.createNode(Scenario.class,scenarioName);
    }

    public void logScenario(String status, String updateText){
        switch (status){
            case TESTFAIL:
                xScenario.fail(updateText);
                break;
            case TESTPASS:
                xScenario.pass(updateText);
                break;
            case TESTWARN:
                xScenario.warning(updateText);
                break;
            case TESTINFO:
                xScenario.info(updateText);
        }
    }

    public void logFeature(String status, String updateText){
        switch (status){
            case TESTFAIL:
                xFeature.fail(updateText);
                break;
            case TESTPASS:
                xFeature.pass(updateText);
                break;
            case TESTWARN:
                xFeature.warning(updateText);
                break;
            case TESTINFO:
                xFeature.info(updateText);
        }
    }

    public void newStep(String stepName, String gherkinStep){
        GherkinKeyword gherkinKeyword = null;
        try {
            gherkinKeyword = new GherkinKeyword(gherkinStep);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        xStep = xScenario.createNode(gherkinKeyword, stepName);
    }

    public void logStep(String status, String updateTekst){
        Markup markup = MarkupHelper.createCodeBlock(updateTekst);
        switch (status){
            case TESTFAIL:
                xStep.fail(updateTekst);
                //addScreenShotToStep("f","errorscreenshot","errorScreen.jpg");
                break;
            case TESTPASS:
                xStep.pass(updateTekst);
                break;
            case TESTWARN:
                xStep.warning(updateTekst);
                break;
            case TESTINFO:
                xStep.info(updateTekst);
        }
    }

    public void logStep(String status, String updateTekst,Throwable errorStacktrace, File screenShot){

        switch (status){
            case TESTFAIL:
                xStep.fail(updateTekst);
                if(errorStacktrace!=null)
                    addStackTrace(errorStacktrace);
                if(screenShot!=null)
                    addScreenShotToStep("f",screenShot.getName(),screenShot);
                break;
            case TESTPASS:
                xStep.pass(updateTekst);
                if(screenShot!=null)
                    addScreenShotToStep("p",screenShot.getName(),screenShot);
                break;
            case TESTWARN:
                xStep.warning(updateTekst);
                break;
            case TESTINFO:
                xStep.info(updateTekst);
        }
    }

    private void addStackTrace(Throwable errStackTrace){
        xStep.fail(errStackTrace);
    }

    public void logTest(List<TestResult> resultList, String testSubject){
        logStep(TESTINFO,testSubject);
        for(TestResult result : resultList){
            logStep(result.getTestStatus(),result.getTimeStamp()+" | "+result.getUpdateText(),result.getErrorStackTrace(),result.getScreenShot());
        }
    }
    
    public void addScreenShotToStep(String status, String updateTekst, File screenShot){
        try{
            String fileName = screenShot.getPath();
            switch (status){
                case TESTFAIL:
                    xStep.fail(updateTekst).addScreenCaptureFromPath(fileName);
                    break;
                case TESTPASS:
                    xStep.pass(updateTekst).addScreenCaptureFromPath(fileName);
                    break;
                case TESTWARN:
                    xStep.warning(updateTekst).addScreenCaptureFromPath(fileName);
                    break;
                case TESTINFO:
                    xStep.info(updateTekst).addScreenCaptureFromPath(fileName);
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
