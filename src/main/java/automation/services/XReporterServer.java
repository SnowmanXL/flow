package automation.services;

import automation.config.TestConfig;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.ExtentXReporter;


/**
 * Created by Wluijk on 2/21/2017.
 */
public class XReporterServer {
    public ExtentReports extent = new ExtentReports();

    public XReporterServer(){
        System.out.println("Initiating XReporterServer");
        ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extent.html");
        htmlReporter.config().setReportName("Version 1.1.0");
        System.out.println("Trying to connect to MongoDB");
        ExtentXReporter extentxReporter = new ExtentXReporter(getMongodIPfromConfig(), 27017);
        extentxReporter.config().setReportName("Test");
        extentxReporter.config().setServerUrl("http://127.0.0.1:1337");
        extent.attachReporter(htmlReporter, extentxReporter);
    }

    private String getMongodIPfromConfig(){
        String mongodIP = null;
        try {
            mongodIP = TestConfig.valueFor("MongodIP");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return mongodIP;
    }

    public ExtentTest createFeature(String featureName){
        ExtentTest feature = extent.createTest(featureName);
        return feature;
    }

}
