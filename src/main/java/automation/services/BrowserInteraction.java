package automation.services;

import automation.config.TestConfig;
import automation.model.TestResult;
import org.apache.commons.io.FileUtils;
import org.json.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static automation.model.TestResult.*;
import static automation.ui.SharedDriver.getOriginalHandle;
import static java.lang.Math.round;
import static org.junit.Assert.assertEquals;

/**
 * Created by Wluijk on 4/12/2017.
 */
public class BrowserInteraction {
    public WebDriver driver;

    private String errScreenName = "errorScreen.jpg";
    private final Logger logger = LoggerFactory.getLogger(BrowserInteraction.class);

    private List<TestResult> testResult = new ArrayList<>();



    public BrowserInteraction(WebDriver webDriver){
        this.driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    public List<TestResult> getTestResult() {
        return testResult;
    }

    public void closeExtraTabs(){
        for(String handle : driver.getWindowHandles()) {
            if (!handle.equals(getOriginalHandle())) {
                driver.switchTo().window(handle);
                driver.close();
            }
        }
        driver.switchTo().window(getOriginalHandle());
    }

    private File takeScreenshot(WebDriver driver){
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(scrFile, new File("target/screenshots/"+"screenshot_"+timeStamp));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return scrFile;
    }

    public void takeScreenshot(String screenShotName, WebDriver driver){
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            logger.info(TestConfig.valueFor("screenShotPath")+screenShotName);
            FileUtils.copyFile(scrFile, new File(TestConfig.valueFor("screenShotPath")+screenShotName)); //TODO: Add configurable path.
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    private By createBy(String locator, String locatorSort){
        By by = null;
        switch (locatorSort) {
            case "xpath":
                by = By.xpath(locator);
                 break;
            case "css":
                by = By.cssSelector(locator);
                break;
            case "id":
                by = By.id(locator);
                break;
        }
        return by;
    }

    private WebElement findElement(By by){
        logger.debug("[m] findElement | by: " + by);
        WebElement element;
        if(checkElementExists(by)){
            testResult.add(new TestResult("Element exists",TESTPASS));
            element = waitForElementToBeVisible(by);
        } else if(waitForElementToExist(by,10)){
            element = driver.findElement(by);
            testResult.add(new TestResult("Element existed within 30 seconds of first try",TESTPASS));
        } else {
            testResult.add(new TestResult("Element "+ by +" has not been fount, returning a null value",TESTWARN));
            logger.warn("[m] findElement | Element "+ by +" has not been fount, returning a null value");
            element = null;
        }
        return element;
    }

    private WebElement findElement(String locator, String locatorSort){
        return findElement(createBy(locator,locatorSort));
    }

    public List<TestResult> loadPortal(String url) {
        //driver workaround, remove cookies
        driver.manage().deleteAllCookies();
        testResult.clear();

        //TODO: ADD CHECK ON REACHABILITY
        boolean reachable = true;

        if(reachable){
            driver.get(url);
            testResult.add(new TestResult("loaded url",TESTPASS ));
        } else {
            testResult.add(new TestResult("url has not been loaded",TESTFAIL));
        }
        return testResult;
    }

    private boolean waitForElementToExist(By by,int maxWaitInSeconds){
        logger.debug("[m] waitForElementToExist | locator: " + by);
        boolean elementFound = false;
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);

        for(int i=0; i<(maxWaitInSeconds*10); i++) {
            if(i%500==0){
                logger.debug("[m] waitForElementToExist | retrycount:"+i+" | waiting 100 milliseconds");
                testResult.add(new TestResult(takeScreenshot(driver),"Element not found | retrycount:"+i, TESTWARN));
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                testResult.add(new TestResult(takeScreenshot(driver),e,TESTFAIL,"Runtime Exception"));
                e.printStackTrace();
            }
            elementFound = checkElementExists(by);
            if(elementFound)
                testResult.add(new TestResult("Wait for element complete: Element Found",TESTPASS));
                break;
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return elementFound;
    }


    private boolean waitForElementToExist(String locator, String locatorSort, int maxWaitInSeconds) {
       return waitForElementToExist(createBy(locator,locatorSort),maxWaitInSeconds);
    }

    private boolean checkElementExists(By by) {
        logger.debug("[m] checkElementExists | locator: " + by);
        return driver.findElements(by).size() != 0;
    }

    public boolean checkElementExists(String locator, String locatorSort) {
        return checkElementExists(createBy(locator,locatorSort));
    }

    public List<TestResult> elementExist(String locator, String locatorSort){
        testResult.clear();
        logger.debug("[m] elementExist | locator : " + locator);
        if (checkElementExists(locator,locatorSort)){
            testResult.add(new TestResult("element exists",TESTPASS));
            return testResult;
        } else {
            logger.error("[m] elementExist | error: element does not exist, screenshot has been taken");
            testResult.add(new TestResult(takeScreenshot(driver),"element does not exist",TESTFAIL));
            return testResult;
        }
    }

    private WebElement waitForElementToBeClickable (final WebElement element){
        logger.debug("[m] waitForElementToBeClickable | locator: " + element);
        return new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(element));
    }

    private WebElement waitForElementToBeClickable (final By by){
        logger.debug("[m] waitForElementToBeClickable | By: " + by);
        return waitForElementToBeClickable(driver.findElement(by));
    }

    private WebElement waitForElementToBeVisible(final WebElement element){
        logger.debug("[m] waitForElementToBeVisible | locator: " + element);
        return new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(element));
    }

    private WebElement waitForElementToBeVisible(final By by){
        logger.debug("[m] waitForElementToBeVisible | locator: " + by);
        return new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public List<TestResult> compareListWithTextOnPage(List<String> compareList, WebElement element){
        testResult.clear();
        logger.debug("[m] compareListWithTextOnPage | compare element" + element + "  | with compare value: " +compareList);

        boolean allElementsAreFound = true;
        for(String attribute : compareList){
            logger.debug("[m] compareListWithTextOnPage | compare attribute" + attribute + "  | with found value: " +element.getText());
            if(!element.getText().contains(attribute)){
                allElementsAreFound = false;
                testResult.add(new TestResult(takeScreenshot(driver),"Comparison failed on attribute " + attribute,TESTFAIL));
                logger.error("Comparison failed on attribute " + attribute);
                break;
            }
        }

        if(allElementsAreFound)
            testResult.add(new TestResult("All elements are equal",TESTPASS));
        return testResult;
    }

    public List<TestResult> compareElementSize(WebElement baseElement, WebElement compareElement, int percentage){
        testResult.clear();

        double fillPercentage = (double) baseElement.getSize().getWidth() / (double) compareElement.getSize().getWidth();
        fillPercentage = round(fillPercentage * 100);

        //do a tricky test to compare the css width %, it should be 65%
        if(fillPercentage==percentage){
            testResult.add(new TestResult("Size of comparison is the expected percentage",TESTPASS));
        } else {
            testResult.add(new TestResult(takeScreenshot(driver),"Size comparison is not the expected percentage",TESTFAIL));
        }
        return testResult;
    }

    public List<TestResult> compareElementValue(String locator, String locatorSort, String compareValue){
        testResult.clear();
        String foundText=getElementText(createBy(locator,locatorSort));
        logger.debug("[m] compareElementValue | compare element" + locator + "  | with compare value: " +compareValue + " found value: "+foundText);

        if(foundText.equals(compareValue)){
            testResult.add(new TestResult("Found text equals compare value", TESTPASS));
            return testResult;
        } else {
            logger.error("[m] compareElementValue | Strings do not match, screenshot has been taken \n foundtext: " + foundText + " | comparison value: " + compareValue);
            testResult.add(new TestResult(takeScreenshot(driver),"Found text does not equal compare value" + foundText + " | comparison value: " + compareValue,TESTFAIL));
            return testResult;
        }
    }

    private String getElementText(By by){
        String elementText=null;
        try {
            elementText= findElement(by).getText();
        } catch (Exception e){
            logger.error("[m] getElementText | Exception found, screenshot has been taken",e);
            testResult.add(new TestResult(takeScreenshot(driver),e,"No text has been found",TESTWARN));
        }
        if(elementText == null){
            logger.warn("[m] getElementText | Null return value converted to \"null\" string");
            testResult.add(new TestResult("Null return value converted to \"null\" string", TESTWARN));
            return "null";
        } else {
            return elementText;
        }
    }

    public List<TestResult> clickElement(WebElement element){
        testResult.clear();
        logger.debug("[m] clickElement | locator: "+ element);
        try {
            waitForElementToBeClickable(element).click();
            testResult.add(new TestResult("element is clickable and has been clicked on",TESTPASS));
            return testResult;
        } catch (Exception e){
            logger.error("[m] clickElement | Exception found, screenshot has been taken",e);
            testResult.add(new TestResult(takeScreenshot(driver),e,"Exception found, unable to to click on element ",TESTFAIL));
            return testResult;
        }
    }

    public List<TestResult> clickElement(String locator, String locatorSort){
        testResult.clear();
        logger.debug("[m] clickElement | locator: "+ locator);
        try {
            waitForElementToBeClickable(findElement(createBy(locator,locatorSort))).click();
            testResult.add(new TestResult("element is clickable and has been clicked on",TESTPASS));
            return testResult;
        } catch (Exception e){
            logger.error("[m] clickElement | Exception found, screenshot has been taken",e);
            testResult.add(new TestResult(takeScreenshot(driver),e,"Exception found, unable to to click on element ",TESTFAIL));
            return testResult;
        }
    }

    public List<TestResult> elementListContains(String locator, String locatorSort, String SearchValue){
        testResult.clear();

        Boolean elementfound=false;
        logger.debug("[m] elementListContains |  Searchvalue: "+SearchValue);
        List<WebElement> webElements = retrievePageObjects(locator,locatorSort);

        if(webElements.isEmpty()){
            logger.warn("[m] elementListContains | No elements found! ");
            testResult.add(new TestResult("elementList is empty!",TESTWARN));
        }

        for (WebElement element : webElements){
            logger.debug(" [m] fillField | Element contains: " + element.getText());
            if(element.getText().contains(SearchValue)){
                elementfound=true;
            }
        }

        if (elementfound){
            testResult.add(new TestResult("element found",TESTPASS));
            return testResult;
        } else {
            testResult.add(new TestResult(takeScreenshot(driver),"element not found",TESTFAIL));
            return testResult;
        }
    }

    public List<TestResult> clickFirstResultOfSearchList(String locator, String locatorSort){
        testResult.clear();
        try {
            waitForElementToBeClickable(retrievePageObjects(locator,locatorSort).get(0)).click();
            testResult.add(new TestResult("first element is clickable and has been clicked on", TESTPASS));
            return testResult;
        } catch (NullPointerException e){
            logger.error("[m] clickFirstResultOfSearchList | exeption! ",e);
            testResult.add(new TestResult(takeScreenshot(driver),e,"exeption found",TESTFAIL));
            return testResult;
        }
    }

    public List<TestResult> compareFirstListResult(String locator, String locatorSort, boolean resultShouldNotBeEmpty){
        testResult.clear();
        if(!"".equals(retrievePageObjects(locator,locatorSort).get(0).getText())){
            testResult.add(new TestResult("resultlist is not empty",TESTPASS));
        } else {
            testResult.add(new TestResult(takeScreenshot(driver),"testresult is empty",TESTFAIL));
        }
        return testResult;
    }

    private List<WebElement> retrievePageObjects(By by){
        return driver.findElements(by);
    }

    private List<WebElement> retrievePageObjects(String locator, String locatorSort){
        return retrievePageObjects(createBy(locator,locatorSort));
    }


    public List<TestResult> fillField(WebElement element, String fillText){
        testResult.clear();
        logger.debug("[m] fillField | element: " + element + " | text: " + fillText);
        if(element.isDisplayed() && element.isEnabled()){
            element.sendKeys(fillText);
            testResult.add(new TestResult("field "+ element + "filled with text: "+ fillText,TESTPASS));
        } else if(!element.isDisplayed()) {
            testResult.add(new TestResult("Element not displayed or enabled, waiting for element",TESTWARN));
            waitForElementToBeVisible(element).sendKeys(fillText);
        } else {
            testResult.add(new TestResult(takeScreenshot(driver),"Error! Screenshot has been taken",TESTFAIL));
        }
        return testResult;
    }


    public List<TestResult> fillField(String locator, String locatorSort, String text){
        testResult.clear();

        logger.debug("[m] fillField | locator: " + locator + " | locatorSort: " + locatorSort + " | text: " + text);
        try {
            findElement(locator, locatorSort).sendKeys(text);
            testResult.add(new TestResult("element found and text inserted",TESTPASS));
            return testResult;
        } catch (NullPointerException e){
            logger.error("[m] fillField | Element with locator: "+ locator + " is not found \n A screenshot has been saved to the report", e);
            testResult.add(new TestResult(takeScreenshot(driver),e,"element is not found", TESTFAIL));
            return testResult;
        }
    }



}
