package automation.services;

import automation.config.TestConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by wilfred on 23-1-17.
 */


public class DBconnection {
    private static TestConfig testConfig;


    //static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    private static final String USER = "postgres";
    private static final String PASS = "root";
    private static Connection conn = null;
    private static java.sql.Statement stmt = null;
    public String result;
    private List<String> queryResultList = new ArrayList<>();
    private int collaborationID;
    private final Logger logger = LoggerFactory.getLogger(DBconnection.class);

    public DBconnection(){
    }

    private String getDbUrl(){
        String DB_URL = null;
        try {
            DB_URL = "jdbc:postgresql://"+ TestConfig.valueFor("PostgresIPaddress")+"/configuration";
            logger.debug("Database URL " + DB_URL);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return DB_URL;
    }


    public String getConfiguration(int id, String column, String table) {
        String statement = "select " + column + " from " + table + " where id = " + id + " limit 1 ";
        executeQuery(statement,column);
        return result;
    }

    public List<String> getPatientTabs(String column, int patientid, String table){
        queryResultList.clear();
        String statement = "select " + column + " from " + table + " where patientid = " + patientid;
        executeQuery(statement, column);
        return queryResultList;
    }

    protected void executeQuery(String statement, String resultCollumn) {
        //String output = null;
        try {
            conn = DriverManager.getConnection(getDbUrl(), USER, PASS);
            stmt = conn.createStatement();
            logger.debug("Statement: "+statement);
            java.sql.ResultSet rs = stmt.executeQuery(statement);
            while (rs.next()) {
                result = rs.getString(resultCollumn);
                queryResultList.add(result);
                logger.debug("Return value: " + result);
            }
            rs.close();
            stmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        //return output;
    }
}
