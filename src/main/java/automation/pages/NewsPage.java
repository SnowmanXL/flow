package automation.pages;

import automation.services.BrowserInteraction;
import automation.services.XReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.Assert.assertTrue;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class NewsPage extends BrowserInteraction {

    @FindBy(id = "mce-EMAIL")
    private WebElement emailField;

    @FindBy(id = "mc-embedded-subscribe")
    private WebElement submitButton;

    public NewsPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void validateSubscribeFunction(XReporter report){
        //tricky test to validate if a new tab is opened
        fillEmailField(report);
        clickSubmitButton(report);

        int numberOfOpenTabs = driver.getWindowHandles().size();
        assertTrue(numberOfOpenTabs==2);

        //Close extra tabs and continue with the original tab
        closeExtraTabs();
    }

    private void fillEmailField(XReporter report){
        report.logTest(fillField(emailField,"test"),"Fill email field");
    }

    private void clickSubmitButton(XReporter report){
        report.logTest(clickElement(submitButton),"click submit button");
    }

}
