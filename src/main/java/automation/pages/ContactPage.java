package automation.pages;

import automation.model.KzaDependance;
import automation.services.BrowserInteraction;
import automation.services.XReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class ContactPage extends BrowserInteraction {

    @FindBy(xpath = "//*[@id=\"post-70\"]/div/div[4]/div/div[3]/div[2]/p")
    private WebElement adressKza;

    private final Logger logger = LoggerFactory.getLogger(ContactPage.class);

    public ContactPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void validateAddressKza(XReporter report){
        report.logTest(compareListWithTextOnPage(new KzaDependance().getAllAttributesAsList(),adressKza) ,"Compare address on site with found address");
    }

}
