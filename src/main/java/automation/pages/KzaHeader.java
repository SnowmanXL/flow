package automation.pages;

import automation.services.BrowserInteraction;
import automation.services.XReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class KzaHeader extends BrowserInteraction {

    private final Logger logger = LoggerFactory.getLogger(KzaHeader.class);

    @FindBy(id = "menu-item-75")
    private WebElement contactButton;

    @FindBy(id = "menu-item-82")
    private WebElement whoAreWeButton;

    @FindBy(id = "menu-item-694")
    private WebElement ourTeamButton;

    @FindBy(xpath = "//*[@id=\"menu-item-649\"]/a")
    private WebElement newsButton;

    public KzaHeader(WebDriver webDriver) {
        super(webDriver);
    }

    public KzaHeader openwebsite(XReporter report){
        report.logTest(loadPortal("http://kza.nl/"),"load kza homepage");
        return new KzaHeader(driver);
    }

    public ContactPage navigateToContactPage(XReporter report){
        report.logTest(clickElement(contactButton),"click submit button");
        return new ContactPage(driver);
    }

    public KzaTeamPage navigateToKzaTeamPage(XReporter report){
        report.logTest(loadPortal("http://kza.nl/wie-zijn-wij/het-kza-team/"),"load kza Teampage");
        return new KzaTeamPage(driver);
    }

    public YoungProfessionalPage navigateToYoungProfessionalPage(XReporter report){
        report.logTest(loadPortal("http://kza.nl/young-professional/"),"load kza Teampage");
        return new YoungProfessionalPage(driver);
    }

    public NewsPage navigateToNewsPage(XReporter report){
        report.logTest(clickElement(newsButton),"click submit button");
        return new NewsPage(driver);
    }

}
