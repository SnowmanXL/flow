package automation.pages;

import automation.model.Employee;
import automation.services.BrowserInteraction;
import automation.services.XReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by Wluijk on 5/29/2017.
 */
public class KzaTeamPage extends BrowserInteraction {

    private final Logger logger = LoggerFactory.getLogger(KzaTeamPage.class);

    @FindBy(xpath = "//*[@id=\"post-676\"]/div/div[3]/div[4]/div/div[2]/div[1]")
    WebElement allEmployeesList;

    @FindBy(xpath = "//*[@id=\"post-676\"]/div/div[3]/div[1]/div/div[2]/div[1]/div[1]/h3")
    WebElement firstEmployee;



    public KzaTeamPage(WebDriver webDriver) {
        super(webDriver);
        logger.debug("first employee = " + firstEmployee.getText());
    }

    public void validateEmployees(XReporter report){
        List<String> employeeList = new ArrayList<>();

        logger.info(allEmployeesList.getText());
        //Stub some data
        employeeList.add(new Employee("Wouter", "Schep").getFullName());
        employeeList.add(new Employee("Arnoud", "Verboom").getFullName());
        employeeList.add(new Employee("Elwin", "Linsen").getFullName());
        employeeList.add(new Employee("Loek", "Muller").getFullName());


        report.logTest(compareListWithTextOnPage(employeeList,allEmployeesList),"Compare employees with site");
    }



}
