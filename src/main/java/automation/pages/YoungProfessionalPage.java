package automation.pages;

import automation.services.BrowserInteraction;
import automation.services.XReporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.Math.round;
import static org.junit.Assert.assertEquals;


/**
 * Created by Wluijk on 5/29/2017.
 */
public class YoungProfessionalPage extends BrowserInteraction {

    private final Logger logger = LoggerFactory.getLogger(KzaTeamPage.class);

    @FindBy(xpath = "//*[@id=\"post-71\"]/div/div[4]/div/div/ul/li[1]/span[2]/span")
    private WebElement progressInBar;

    @FindBy(xpath = "//*[@id=\"post-71\"]/div/div[4]/div/div/ul/li[1]/span[2]")
    private WebElement progressBar;

    public YoungProfessionalPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void validateProgressBar(XReporter reporter){
        reporter.logTest(compareElementSize(progressInBar,progressBar,65),"Determine fill % of progressbar");
    }


}
