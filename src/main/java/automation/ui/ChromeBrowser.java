package automation.ui;

import java.util.concurrent.TimeUnit;

import automation.config.TestConfig;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

class ChromeBrowser extends ChromeDriver {
    public static WebDriver buildChromeBrowser() throws Throwable {
    	System.setProperty("webdriver.chrome.driver", TestConfig.valueFor("WebDriverChromeDriverPath"));
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
    	ChromeBrowser browser = new ChromeBrowser(options);
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return browser;
    }
    
    private ChromeBrowser(ChromeOptions options) {
    	super(options);
    }
}
