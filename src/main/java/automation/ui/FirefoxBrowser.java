package automation.ui;

import automation.config.TestConfig;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;

import java.util.concurrent.TimeUnit;

public class FirefoxBrowser extends FirefoxDriver {

    public static FirefoxBrowser buildFirefoxBrowser() throws Throwable {
        System.setProperty("webdriver.gecko.driver", TestConfig.valueFor("WebDriverFirefoxDriverPath"));
        FirefoxProfile ffprofile = new ProfilesIni().getProfile("Selenium");
        ffprofile.setAcceptUntrustedCertificates(true);

        FirefoxBrowser browser = new FirefoxBrowser(ffprofile);
        browser.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return browser;
    }

    private FirefoxBrowser(FirefoxProfile desiredProfile) {
        super(desiredProfile);
    }
}
